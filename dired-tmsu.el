;;; dired-tmsu.el --- Emacs dired frontend for tmsu semantic tagging -*- lexical-binding: t; -*-

;; Copyright (C) 2022 Nils Grunwald <github.com/ngrunwald>
;; Author: Nils Grunwald
;; URL: https://github.com/ngrunwald/dired-tmsu.el
;; Created: 2022
;; Version: 0.1.0
;; Keywords: tmsu filesystem tags
;; Package-Requires: ((s "20210616.619"))

;; This file is NOT part of GNU Emacs.

;; dired-tmsu.el is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; dired-tmsu.el is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with dired-tmsu.el.
;; If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This package provides an Emacs dired frontend for tmsu tagging and navigating
;; query resultys in dired.

;;; Code:
(require 'seq)
(require 's)
(require 'subr-x)
(require 'cl-lib)
(require 'dired)
(require 'thingatpt)
(require 'shell)

(defgroup dired-tmsu nil
  "dired-tmsu customization group."
  :prefix "dired-tmsu-"
  :group 'external)

(defcustom dired-tmsu-program "tmsu" "Name of the tmsu program"
  :type 'string)

(defvar dired-tmsu-query-history '())

(defvar dired-tmsu-freetag-history '())

(defvar dired-tmsu-tags-cache '())

(defvar-local dired-tmsu-query-params nil)

(defun database-found-p (s)
  (if (s-match "tmsu: no database found" s)
      nil t))

(defun dired-tmsu--find-root ()
  (let ((ret (s-lines (shell-command-to-string (format "%s info" dired-tmsu-program)))))
    (when (> (length ret) 1)
      (let* ((root-path-str (cadr ret))
             (path (s-trim (cadr (s-split ": " root-path-str)))))
        path))))

(defun dired-tmsu--parse-result (res)
  (if (s-match "^tmsu:" res)
      (progn
        (message res)
        nil)
    (thread-last res
      (s-trim)
      (s-lines)
      (seq-remove 's-blank-p))))

(defun dired-tmsu--list-all-tags ()
  (let* ((db-root (or (car (assoc default-directory dired-tmsu-tags-cache (lambda (root dir) (s-match (regexp-quote root) dir))))
                      (dired-tmsu--find-root)))
         (cache (alist-get db-root dired-tmsu-tags-cache nil nil 'string=)))
    (if cache
        cache
      (let ((tags (dired-tmsu--parse-result (shell-command-to-string (format "%s tags -1 --name=never" dired-tmsu-program)))))
        (push `(,db-root . ,tags) dired-tmsu-tags-cache)
        tags))))

(defun dired-tmsu--list-all-values (&optional tag)
  (let ((clean-tag (or tag "")))
    (dired-tmsu--parse-result (shell-command-to-string (format "%s values -1 %s" dired-tmsu-program (shell-quote-argument clean-tag))))))

(defun dired-tmsu--list-file-tags (file)
  (dired-tmsu--parse-result (shell-command-to-string (format "%s tags -1 --name=never %s" dired-tmsu-program (shell-quote-argument file)))))

(defun dired-tmsu--list-files-tags (files)
  (seq-uniq (seq-mapcat (lambda (f) (dired-tmsu--list-file-tags f)) files)))

;;;###autoload
(defun dired-tmsu-marked-files-tags (&optional arg)
  (interactive)
  (let* ((files (dired-get-marked-files nil arg nil nil t))
         (tags (dired-tmsu--list-files-tags files)))
    (message "FILETAGS -> %s" (s-join " " tags))))

;;;###autoload
(defun dired-tmsu-list-untagged (&optional arg)
  (interactive "P")
  (let ((result (dired-tmsu--parse-result
                 (shell-command-to-string (if arg
                                              (format "%s untagged %s" dired-tmsu-program (shell-quote-argument default-directory))
                                            (format "%s untagged" dired-tmsu-program))))))
    (when result
      (dired (cons "dired-tmsu - untagged" result)))))

;;;###autoload
(defun dired-tmsu-delete-tag ()
  (interactive)
  (let ((selected-tag (dired-tmsu--completing-read-tag)))
    (setq dired-tmsu-tags-cache
          (assoc-delete-all default-directory dired-tmsu-tags-cache (lambda (root dir) (s-match (regexp-quote root) dir))))
    (shell-command-to-string (format "%s delete %s" dired-tmsu-program (shell-quote-argument selected-tag)))))

;;;###autoload
(defun dired-tmsu-remove-tag-from-marked-files (&optional arg)
  (interactive "P")
  (let* ((files (dired-get-marked-files nil arg nil nil t))
         (tags (dired-tmsu--list-files-tags files))
         (selected-tag (completing-read "Tag to remove " tags nil t)))
    (seq-each (lambda (file) (shell-command-to-string (format "%s untag %s '%s'"
                                                              dired-tmsu-program
                                                              (shell-quote-argument file)
                                                              selected-tag)))
              files)))

(defun dired-tmsu--completing-read-tags ()
  (let ((all-tags (dired-tmsu--list-all-tags)))
    (cl-loop for tag = (completing-read "Tag: " all-tags nil nil)
             until (s-blank-p tag)
             collect tag)))

(defun dired-tmsu--completing-read-tag ()
  (completing-read "Tag: " (dired-tmsu--list-all-tags) nil nil))

(defun dired-tmsu--completing-read-value (tag)
  (let ((candidates (dired-tmsu--list-all-values tag)))
    (when candidates
      (completing-read "Value: " candidates nil nil))))

(defun dired-tmsu--purge-tags-cache-if-new (msg)
  (when (s-match "new tag" msg)
    (setq dired-tmsu-tags-cache
          (assoc-delete-all default-directory dired-tmsu-tags-cache (lambda (root dir) (s-match (regexp-quote root) dir)))))
  msg)

;;;###autoload
(defun dired-tmsu-tag-marked-files (&optional arg)
  (interactive "P")
  (let* ((files (dired-get-marked-files nil arg nil nil t))
         (tag (dired-tmsu--completing-read-tag))
         (value (when (not (s-contains-p "=" tag))
                  (dired-tmsu--completing-read-value tag))))
    (seq-each (lambda (f)
                (thread-first (shell-command-to-string
                               (if value
                                   (format "%s tag %s %s=%s"
                                           dired-tmsu-program
                                           (shell-quote-argument f)
                                           (shell-quote-argument tag)
                                           (shell-quote-argument value))
                                 (format "%s tag %s %s"
                                         dired-tmsu-program
                                         (shell-quote-argument f)
                                         (shell-quote-argument tag))))
                              (dired-tmsu--purge-tags-cache-if-new)))
              files)))

(defun dired-tmsu--list-query-files (query &optional global?)
  (let* ((result (s-trim (shell-command-to-string (if global?
                                                      (format "%s files '%s'" dired-tmsu-program query)
                                                    (format "%s files '--path=%s' '%s'" dired-tmsu-program default-directory query)))))
         (file-paths (s-lines result)))
    (if (s-match "^tmsu: no such tag" (car file-paths))
        (message (s-join " - " file-paths))
      (progn
        (dired (cons (concat "dired-tmsu - " query) file-paths))
        (setq-local dired-tmsu-query-params (list query global?))))))

(defvar dired-tmsu-tag-chars "[:alnum:]-_/:\\")

(define-thing-chars dired-tmsu-tag dired-tmsu-tag-chars)

(defun dired-tmsu-completion-at-point (&optional query?)
  (interactive)
  (let* ((bds (bounds-of-thing-at-point 'dired-tmsu-tag))
         (start (or (car bds) (point)))
         (end (or (cdr bds) (point)))
         (preceding (string (char-before start))))
    (if (string= "=" preceding)
        (let* ((tag-pos (- start 2))
               (tag (save-excursion (goto-char tag-pos) (thing-at-point 'dired-tmsu-tag))))
          (list start end (completion-table-dynamic (lambda (_) (dired-tmsu--list-all-values tag)))))
      (list start end (completion-table-dynamic
                       (lambda (_)
                         (seq-concatenate 'list (dired-tmsu--list-all-tags) (when query? '("and" "or" "not")))))))))

(defun dired-tmsu-query-completion-at-point ()
    (dired-tmsu-completion-at-point t))

(defun dired-tmsu--read-tag-expr (prompt &optional history default-content query?)
  (let* ((map (copy-keymap minibuffer-local-shell-command-map))
         (completion-at-point-functions (if query?
                                            '(dired-tmsu-query-completion-at-point)
                                            '(dired-tmsu-completion-at-point))))
    (set-keymap-parent map minibuffer-local-map)
    (define-key map (kbd "[?\t]") #'completion-at-point)
    (read-from-minibuffer prompt nil map nil history default-content)))

(defun tmsu--freetag-command-string (file &optional initial-tags)
  (let* ((current-tags (when (f-exists? file) (dired-tmsu--list-file-tags file)))
         (tag-expr (dired-tmsu--read-tag-expr "tmsu tags: " 'dired-tmsu-freetag-history (s-join " " (seq-union initial-tags current-tags)))))
    (message "HERE")
    (format "%s tag %s %s"
            dired-tmsu-program
            (shell-quote-argument file)
            tag-expr)))

(defun tmsu--freetag-file (file &optional initial-tags)
  (thread-first (tmsu--freetag-command-string file initial-tags)
                (dired-tmsu--purge-tags-cache-if-new)))

;;;###autoload
(defun dired-tmsu-list-query-files (&optional arg)
  (interactive "P")
  (let ((query (dired-tmsu--read-tag-expr "tmsu query: " 'dired-tmsu-query-history nil t)))
    (dired-tmsu--list-query-files query arg)))

;;;###autoload
(defun dired-tmsu-freetag-marked-files (&optional arg)
  (interactive "P")
  (let* ((files (dired-get-marked-files nil arg nil nil t))
         (current-tags (if (= 1 (length files))
                           (dired-tmsu--list-file-tags (car files))
                         nil))
         (tag-expr (dired-tmsu--read-tag-expr "tmsu tags: " 'dired-tmsu-freetag-history (s-join " " current-tags)))
         (files-list (s-join " " (seq-map (lambda (f) (shell-quote-argument f)) files))))
    (thread-first (shell-command-to-string (format "%s tag --tags=\"%s\" %s"
                                                   dired-tmsu-program
                                                   tag-expr
                                                   files-list))
                  (dired-tmsu--purge-tags-cache-if-new))))

(provide 'dired-tmsu)
;;; dired-tmsu.el ends here
